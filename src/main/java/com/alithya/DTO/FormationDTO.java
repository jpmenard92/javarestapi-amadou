package com.alithya.DTO;

import com.alithya.entity.Formation;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
public class FormationDTO {

    private  Long id;

    private String libelleFormation;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<SiteDTO> sites = new ArrayList<>();

    public static FormationDTO fromEntity(Formation formation) {
        if (formation == null) {
            return null;
        }
        return FormationDTO.builder()
                .id(formation.getId())
                .libelleFormation(formation.getLibelleFormation())
               .sites(
                        formation.getSites() != null ?
                                formation.getSites()
                                        .stream()
                                        .map(SiteDTO::fromEntityNotFormation)
                                        .collect(Collectors.toList()) : null
                )
                .build();
    }
    public static FormationDTO fromEntityNotSite(Formation formation) {
        if (formation == null) {
            return null;
        }
        return FormationDTO.builder()
                .id(formation.getId())
                .libelleFormation(formation.getLibelleFormation())
                .build();
    }

    public static Formation toEntity(FormationDTO formationDTO) {
        if (formationDTO == null) {
            return null;
        }
        Formation formation = new Formation();
        formation.setId(formationDTO.getId());
        formation.setLibelleFormation(formationDTO.getLibelleFormation());
        return formation;
    }
}
