package com.alithya.DTO;

import com.alithya.entity.Site;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class SiteDTO {

    private Long id;

    private String nom;

    private String url;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<FormationDTO> formations;

    public static SiteDTO fromEntity(Site site) {
        if (site == null) {
            return null;
        }
        return SiteDTO.builder()
                .id(site.getId())
                .nom(site.getNom())
                .url(site.getUrl())
                .formations(
                        site.getFormations() != null ?
                                site.getFormations()
                                        .stream()
                                        .map(FormationDTO::fromEntityNotSite)
                                        .collect(Collectors.toList()) : null
                )
                .build();
    }

    public static SiteDTO fromEntityNotFormation(Site site) {
        if (site == null) {
            return null;
        }
        return SiteDTO.builder()
                .id(site.getId())
                .nom(site.getNom())
                .url(site.getUrl())
                .build();
    }

    public static Site toEntity(SiteDTO siteDTO) {
        if (siteDTO == null) {
            return null;
        }
        Site site = new Site();
        site.setId(siteDTO.getId());
        site.setNom(siteDTO.getNom());
        site.setUrl(siteDTO.getUrl());
        return site;
    }
}
