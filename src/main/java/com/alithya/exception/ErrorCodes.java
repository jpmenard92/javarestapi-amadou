package com.alithya.exception;

import lombok.Getter;

@Getter
public enum ErrorCodes {

  SITE_NOT_FOUND(1000),
  FORMATION_NOT_FOUND(1002),
  SITE_EXISTS_WITH_FORMATIONS(1001),
  SITE_NOT_VALID(12001);

  private int code;

  ErrorCodes(int code) {
    this.code = code;
  }


}
