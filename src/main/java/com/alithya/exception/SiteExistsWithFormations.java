package com.alithya.exception;

import lombok.Getter;

public class SiteExistsWithFormations  extends  RuntimeException{
    @Getter
    private ErrorCodes errorCode;

    public SiteExistsWithFormations(String message, ErrorCodes errorCode){
        super(message);
        this.errorCode=errorCode;
    }
}
