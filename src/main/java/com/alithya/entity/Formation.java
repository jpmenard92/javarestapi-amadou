package com.alithya.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "formations")
public class Formation implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "libelle_formation",nullable = false)
    private String libelleFormation;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(
            name = "formation_offertes",
            joinColumns = { @JoinColumn(name = "idformation",referencedColumnName = "id",
                    nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "idsite",
                    referencedColumnName = "id",
                    nullable = false, updatable = false) }
    )
    private List<Site> sites = new ArrayList<>();
}
