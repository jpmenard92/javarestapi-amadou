package com.alithya.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@Entity
@Table(name = "site")
public class Site   implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nom;

    private String url;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinTable(
            name = "formation_offertes",
            joinColumns = { @JoinColumn(name = "idsite",referencedColumnName = "id",
                    nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "idformation",
                    referencedColumnName = "id",
                    nullable = false, updatable = false) }
    )
    private List<Formation> formations = new ArrayList<>();

}
