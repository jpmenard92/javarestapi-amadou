package com.alithya.controller;

import com.alithya.DTO.FormationDTO;
import com.alithya.service.FormationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/api/v1/formations")
public class FormationController {

   private  FormationService formationService;

    @Autowired
    public FormationController(FormationService formationService){
        this.formationService=formationService;
    }

    /**
     * Get all formations list.
     *
     * @return the list
     */
    @GetMapping
    public List<FormationDTO> getAllFormations() {
        return formationService.getAllFormations();
    }

    /**
     * Create a une formation.
     *
     * @param formationDTO
     * @return the formation
     */
    @PostMapping
    public FormationDTO createFormation(@RequestBody FormationDTO formationDTO) {
        return formationService.save(formationDTO);
    }

    /**
     * Trouver une formation
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public FormationDTO getFormationById(@PathVariable("id") Long id){
        return formationService.getFormationById(id);
    }
}
