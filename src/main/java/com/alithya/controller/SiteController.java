package com.alithya.controller;

import com.alithya.DTO.SiteDTO;
import com.alithya.service.SiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/sites")
public class SiteController {

    private SiteService siteService;

    @Autowired
    public SiteController(SiteService siteService){
        this.siteService=siteService;
    }

    /**
     * Get all sites list.
     *
     * @return the list
     */
    @GetMapping
    public List<SiteDTO> getAllSites() {
        return siteService.findAll();
    }

    /**
     * Get a site by Id.
     *
     * @return the site
     */
    @GetMapping("/{id}")
    public SiteDTO getSiteById(@PathVariable("id") Long id) {
        return siteService.findSiteById(id);
    }

    /**
     * Create or clone a site.
     *
     * @param siteDTO
     * @return the site
     */
    @PostMapping
    public SiteDTO createSite(@RequestBody SiteDTO siteDTO,@RequestParam(name = "clone", required = false) Optional<Boolean> isClone) {

        if(isClone.isPresent() && isClone.get()){
            return siteService.clonerSite(siteDTO);
        }
        else{
            return siteService.save(siteDTO);

        }

    }

    /**
     * Update site
     * @param siteDTO
     * @return the site
     */
    @PutMapping
    public SiteDTO updateSite (@RequestBody SiteDTO siteDTO){
        return siteService.updateSite(siteDTO);
    }
}
