package com.alithya.validator;

import com.alithya.DTO.FormationDTO;
import org.springframework.util.StringUtils;
import java.util.ArrayList;
import java.util.List;

public class FormationValidator {

  public static List<String> validate(FormationDTO formationDTO) {
    List<String> errors = new ArrayList<>();

    if (formationDTO == null) {
      errors.add("Veuillez renseigner le libelle de la formation");
      return errors;
    }

    if (!StringUtils.hasLength(formationDTO.getLibelleFormation())) {
      errors.add("Veuillez renseigner le libelle de la formation");
    }
    return errors;
  }

}
