package com.alithya.validator;

import com.alithya.DTO.SiteDTO;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class SiteValidator {

  public static List<String> validate(SiteDTO siteDTO) {
    List<String> errors = new ArrayList<>();

    if (siteDTO == null) {
      errors.add("Veuillez renseigner le nom du site");
      errors.add("Veuillez renseigner l'url du site");
      return errors;
    }

    if (!StringUtils.hasLength(siteDTO.getNom())) {
      errors.add("Veuillez renseigner le nom du site");
    }
    if (!StringUtils.hasLength(siteDTO.getNom())) {
      errors.add("Veuillez renseigner l'url du site");
    }
    return errors;
  }

}
