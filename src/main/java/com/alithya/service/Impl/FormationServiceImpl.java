package com.alithya.service.Impl;

import com.alithya.DTO.FormationDTO;
import com.alithya.entity.Formation;
import com.alithya.entity.Site;
import com.alithya.exception.EntityNotFoundException;
import com.alithya.exception.ErrorCodes;
import com.alithya.repository.FormationRepository;
import com.alithya.repository.SiteRepository;
import com.alithya.service.FormationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class FormationServiceImpl implements FormationService {


    private FormationRepository formationRepository;
    private SiteRepository siteRepository;

    @Autowired
    public FormationServiceImpl(FormationRepository formationRepository
    ,SiteRepository siteRepository){
        this.formationRepository=formationRepository;
        this.siteRepository=siteRepository;
    }

    /**
     * Enregistrer une formation
     * @param dto
     * @return
     */
    @Override
    public FormationDTO save (FormationDTO dto) {

        List<String> siteErrors = new ArrayList<>();
        Formation formation = formationRepository.save(FormationDTO.toEntity(dto));
        List<Site> listeSites= new ArrayList<>();
        if (dto.getSites() != null) {
            dto.getSites().forEach(sites -> {
                if (sites != null) {
                    Optional<Site> site= siteRepository.findById(sites.getId());
                    if (site.isEmpty()) {
                        siteErrors.add("Le site avec l'ID " +sites.getId() + " n'existe pas");
                    }
                    listeSites.add(site.get());

                } else {
                    siteErrors.add("Impossible d'enregister une formation avec une site nul");
                }
            });
            formation.getSites().addAll(listeSites);
            formationRepository.save(formation);
        }
        return FormationDTO.fromEntity(formation);
    }

    /**
     * Lister tous les formations
     * @return
     */
    @Override
    public List<FormationDTO> getAllFormations() {
        return formationRepository.findAll().stream()
                .map(FormationDTO::fromEntityNotSite)
                .collect(Collectors.toList());
    }

    /**
     * Trouver une formation
     * @param id
     * @return
     */
    @Override
    public FormationDTO getFormationById(Long id) {
        if (id == null) {
            log.error("Site is is not found");
            return null;
        }
        return formationRepository.findById(id)
                .map(FormationDTO::fromEntity)
                .orElseThrow(() -> new EntityNotFoundException(
                        "Aucun formation avec l'id = " + id + " n' ete trouve",
                        ErrorCodes.FORMATION_NOT_FOUND)
                );
    }
}
