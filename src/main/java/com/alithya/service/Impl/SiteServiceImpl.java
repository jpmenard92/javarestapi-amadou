package com.alithya.service.Impl;

import com.alithya.DTO.SiteDTO;
import com.alithya.entity.Formation;
import com.alithya.entity.Site;
import com.alithya.exception.EntityNotFoundException;
import com.alithya.exception.ErrorCodes;
import com.alithya.exception.InvalidEntityException;
import com.alithya.repository.FormationRepository;
import com.alithya.repository.SiteRepository;
import com.alithya.service.SiteService;
import com.alithya.validator.FormationValidator;
import com.alithya.validator.SiteValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class SiteServiceImpl implements SiteService {

    private SiteRepository siteRepository;
    private FormationRepository formationRepository;

    @Autowired
    public SiteServiceImpl( SiteRepository siteRepository,FormationRepository formationRepository){
        this.siteRepository = siteRepository;
        this.formationRepository = formationRepository;
    }

    /**
     * Lister tous les sites
     * @return
     */
    @Override
    public List<SiteDTO> findAll() {
        return siteRepository.findAll().stream()
                .map(SiteDTO::fromEntityNotFormation)
                .collect(Collectors.toList());
    }

    /**
     * Enregistrer un site
     * @param siteDTO
     * @return
     */
    @Override
    public SiteDTO save(SiteDTO siteDTO) {
        List<String> errors = SiteValidator.validate(siteDTO);

        if (!errors.isEmpty()) {
            log.error("Le site  n'est pas valide{}", siteDTO);
            throw new InvalidEntityException("Le site  n'est pas valide", ErrorCodes.SITE_NOT_VALID, errors);
        }

        Site site = siteRepository.save(SiteDTO.toEntity(siteDTO));
        List<Formation> listeFormations = new ArrayList<>();
        if (siteDTO.getFormations() != null) {
            siteDTO.getFormations().forEach(formations -> {
                List<String>   formationsErrors = FormationValidator.validate(formations);
                if (formations != null) {
                    Optional<Formation> formation = formationRepository.findById(formations.getId());
                    if (formation.isEmpty()) {
                        formationsErrors.add("La formation avec l'ID " +formations.getId() + " n'existe pas");
                    }
                    listeFormations.add(formation.get());

                } else {
                    formationsErrors.add("Impossible d'enregister un site avec une formation nulle");
                }
            });
            site.getFormations().addAll(listeFormations);
            siteRepository.save(site);
        }
        return SiteDTO.fromEntity(site);
    }


    /**
     * Trouver un site avec l'Id
     * @param id
     * @return
     */
    @Override
    public SiteDTO findSiteById(Long id) {
        if (id == null) {
            log.error("Site is is not found");
            return null;
        }
        return siteRepository.findById(id)
                .map(SiteDTO::fromEntity)
                .orElseThrow(() -> new EntityNotFoundException(
                        "Aucun site avec l'id = " + id + " n' ete trouve",
                        ErrorCodes.SITE_NOT_FOUND)
                );
    }

    /**
     * Modifier le site
     * @param siteDTO
     * @return
     */
    @Override
    public SiteDTO updateSite(SiteDTO siteDTO) {
        Optional<Site> siteOptional = siteRepository.findById(siteDTO.getId());
        if (siteOptional.isEmpty()) {
            log.warn("Aucun site n'a ete trouve" + siteDTO.getId());
            throw new EntityNotFoundException("Aucun site n'a ete trouve  " + siteDTO.getId(), ErrorCodes.SITE_NOT_FOUND);
        }
        Site site = siteOptional.get();
        site.setNom(siteDTO.getNom());
        site.setUrl(siteDTO.getUrl());
        return  SiteDTO.fromEntity(
                siteRepository.save(site)
        );
    }


    /**
     * Cloner le site
     * @return
     */
    @Override
    public SiteDTO clonerSite(SiteDTO siteDTO) {
        List<String> formationsErrors = new ArrayList<>();
        siteDTO = findSiteById(siteDTO.getId());
        SiteDTO siteDTO1 = new SiteDTO();
        siteDTO1.setNom(siteDTO.getNom());
        siteDTO1.setUrl(siteDTO.getUrl());
        siteDTO1.setFormations(siteDTO.getFormations());
        System.out.println("------------------------------------"+siteDTO1.getFormations());
        Site site = siteRepository.save(SiteDTO.toEntity(siteDTO1));
        List<Formation> listeFormations = new ArrayList<>();
        if (siteDTO1.getFormations() != null) {
            siteDTO1.getFormations().forEach(formations -> {
                if (formations != null) {
                    Optional<Formation> formation = formationRepository.findById(formations.getId());
                    if (formation.isEmpty()) {
                        formationsErrors.add("La formation avec l'ID " +formations.getId() + " n'existe pas");
                    }
                    listeFormations.add(formation.get());


                } else {
                    formationsErrors.add("Impossible d'enregister un site avec une formation nulle");
                }
            });
            site.getFormations().addAll(listeFormations);
            siteRepository.save(site);
        }
        return SiteDTO.fromEntity(site);
    }
}
