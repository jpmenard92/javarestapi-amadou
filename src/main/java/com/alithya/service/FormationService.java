package com.alithya.service;

import com.alithya.DTO.FormationDTO;

import java.util.List;

public interface FormationService {

    FormationDTO save (FormationDTO dto);

    List<FormationDTO> getAllFormations();

    FormationDTO getFormationById(Long id);
}
