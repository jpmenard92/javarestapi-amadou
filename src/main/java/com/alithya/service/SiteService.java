package com.alithya.service;

import com.alithya.DTO.SiteDTO;

import java.util.List;


public interface SiteService {

    List<SiteDTO> findAll();

    SiteDTO save(SiteDTO site);

    SiteDTO findSiteById(Long id);

    SiteDTO updateSite(SiteDTO siteDTO);

    SiteDTO clonerSite(SiteDTO siteDTO);
}
